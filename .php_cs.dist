<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

return Config::create()
    ->setRiskyAllowed(true)
    ->setFinder(
        Finder::create()
            ->in(__DIR__)
            ->path('api/')
            ->notPath('api/cache/')
            ->notPath('api/vendor/')
    )
    ->setRules([
        'align_multiline_comment' => ['comment_type' => 'all_multiline'],
        'array_syntax' => ['syntax' => 'short'],
        'binary_operator_spaces' => ['default' => 'single_space'],
        'blank_line_after_namespace' => true,
        'blank_line_after_opening_tag' => true,
        'blank_line_before_statement' => [
            'statements' => [
                'break',
                'continue',
                'declare',
                'die',
                'do',
                'exit',
                'for',
                'foreach',
                'goto',
                'if',
                'include',
                'include_once',
                'require',
                'require_once',
                'return',
                'switch',
                'throw',
                'try',
                'while',
                'yield',
            ],
        ],
        'braces' => [
            'allow_single_line_closure' => false,
            'position_after_anonymous_constructs' => 'same',
            'position_after_control_structures' => 'same',
            'position_after_functions_and_oop_constructs' => 'next',
        ],
        'cast_spaces' => ['space' => 'single'],
        'class_definition' => [
            'multiLineExtendsEachSingleLine' => true,
            'singleItemSingleLine' => true,
            'singleLine' => false,
        ],
        'class_keyword_remove' => false,
        'combine_consecutive_issets' => true,
        'combine_consecutive_unsets' => true,
        'concat_space' => ['spacing' => 'one'],
        'declare_equal_normalize' => ['space' => 'none'],
        'declare_strict_types' => true,
        'dir_constant' => true,
        'doctrine_annotation_array_assignment' => false,
        'doctrine_annotation_braces' => false,
        'doctrine_annotation_indentation' => false,
        'doctrine_annotation_spaces' => false,
        'elseif' => true,
        'encoding' => true,
        'ereg_to_preg' => true,
        'full_opening_tag' => true,
        'function_declaration' => ['closure_function_spacing' => 'one'],
        'function_to_constant' => [
            'functions' => [
                'get_class',
                'php_sapi_name',
                'phpversion',
                'pi',
            ],
        ],
        'function_typehint_space' => true,
        'general_phpdoc_annotation_remove' => [
            'annotations' => [
                'author',
                'expectedException',
                'expectedExceptionCode',
                'expectedExceptionMessage',
                'expectedExceptionMessageRegExp',
                'large',
                'medium',
                'small',
                'test',
                'testdox',
                'ticket',
            ],
        ],
        // NOTE Adds comments to header of all files
        'header_comment' => false,
        'heredoc_to_nowdoc' => true,
        'include' => true,
        'indentation_type' => true,
        'is_null' => ['use_yoda_style' => false],
        'line_ending' => true,
        'linebreak_after_opening_tag' => true,
        'list_syntax' => ['syntax' => 'short'],
        'lowercase_cast' => true,
        'lowercase_constants' => true,
        'lowercase_keywords' => true,
        'magic_constant_casing' => true,
        'mb_str_functions' => false,
        'method_argument_space' => [
            'keep_multiple_spaces_after_comma' => false,
        ],
        'method_separation' => true,
        'modernize_types_casting' => true,
        'native_function_casing' => true,
        'native_function_invocation' => false,
        'new_with_braces' => true,
        'no_alias_functions' => true,
        'no_blank_lines_after_class_opening' => true,
        'no_blank_lines_after_phpdoc' => true,
        'no_blank_lines_before_namespace' => false,
        'no_break_comment' => ['comment_text' => 'no break'],
        'no_closing_tag' => true,
        'no_empty_comment' => true,
        'no_empty_phpdoc' => true,
        'no_empty_statement' => true,
        'no_extra_consecutive_blank_lines' => [
            'tokens' => [
                // NOTE Removes line after break in switch
                //'break',
                'case',
                'continue',
                'curly_brace_block',
                'default',
                'extra',
                'parenthesis_brace_block',
                // NOTE Removes line after return before case in switch
                //'return',
                'square_brace_block',
                'switch',
                'throw',
                'use',
                'use_trait',
            ],
        ],
        'no_homoglyph_names' => true,
        'no_leading_import_slash' => true,
        'no_leading_namespace_whitespace' => true,
        'no_mixed_echo_print' => ['use' => 'echo'],
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_multiline_whitespace_before_semicolons' => true,
        'no_null_property_initialization' => true,
        'no_php4_constructor' => true,
        'no_short_bool_cast' => true,
        'no_short_echo_tag' => true,
        'no_singleline_whitespace_before_semicolons' => true,
        'no_spaces_after_function_name' => true,
        'no_spaces_around_offset' => [
            'positions' => ['inside', 'outside'],
        ],
        'no_spaces_inside_parenthesis' => true,
        'no_superfluous_elseif' => true,
        'no_unneeded_curly_braces' => true,
        'no_trailing_comma_in_list_call' => true,
        'no_trailing_comma_in_singleline_array' => true,
        'no_trailing_whitespace' => true,
        'no_trailing_whitespace_in_comment' => true,
        'no_unneeded_control_parentheses' => [
            'statements' => [
                'break',
                'clone',
                'continue',
                'echo_print',
                'return',
                'switch_case',
                'yield',
            ],
        ],
        'no_unneeded_final_method' => true,
        'no_unreachable_default_argument_value' => true,
        'no_unused_imports' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'no_whitespace_before_comma_in_array' => true,
        'no_whitespace_in_blank_line' => true,
        'non_printable_character' => ['use_escape_sequences_in_strings' => false],
        // NOTE Replaces "{" and "}" with "[" and "]" for strings
        'normalize_index_brace' => false,
        'not_operator_with_space' => false,
        'not_operator_with_successor_space' => false,
        'object_operator_without_whitespace' => true,
        // TODO There is no way to leave inherited methods on top
        //'ordered_class_elements' => [
        //    'order' => [
        //        'use_trait',
        //        'constant_public',
        //        'constant_protected',
        //        'constant_private',
        //        'property_public_static',
        //        'property_protected_static',
        //        'property_private_static',
        //        'property_static',
        //        'property_public',
        //        'property_protected',
        //        'property_private',
        //        'method_public_static',
        //        'method_protected_static',
        //        'method_private_static',
        //        'construct',
        //        'destruct',
        //        'magic',
        //        'phpunit',
        //        'method_public',
        //        'method_protected',
        //        'method_private',
        //    ],
        //],
        'ordered_imports' => [
            'importsOrder' => [
                'class',
                'const',
                'function',
            ],
            'sortAlgorithm' => 'alpha',
        ],
        'php_unit_construct' => [
            'assertions' => [
                'assertEquals',
                'assertNotEquals',
                'assertNotSame',
                'assertSame',
            ],
        ],
        'php_unit_fqcn_annotation' => true,
        'php_unit_test_class_requires_covers' => false,
        'phpdoc_add_missing_param_annotation' => ['only_untyped' => true],
        'phpdoc_align' => ['tags' => []],
        'phpdoc_annotation_without_dot' => true,
        'phpdoc_indent' => true,
        'phpdoc_inline_tag' => true,
        'phpdoc_no_access' => true,
        'phpdoc_no_alias_tag' => [
            'replacements' => [
                'type' => 'var',
                'link' => 'see',
            ],
        ],
        'phpdoc_no_package' => true,
        'phpdoc_no_useless_inheritdoc' => false,
        'phpdoc_order' => true,
        'phpdoc_return_self_reference' => [
            'replacements' => [
                'this' => '$this',
                '@this' => '$this',
                '$self' => 'static',
                '@self' => 'static',
                '$static' => 'static',
                '@static' => 'static',
            ],
        ],
        'phpdoc_scalar' => true,
        'phpdoc_separation' => false,
        'phpdoc_single_line_var_spacing' => true,
        'phpdoc_summary' => false,
        'phpdoc_to_comment' => false,
        'phpdoc_trim' => true,
        'phpdoc_types' => true,
        'phpdoc_types_order' => [
            'null_adjustment' => 'always_last',
            // TODO Requires phpDocs refactoring
            //'sort_algorithm' => 'alpha',
            'sort_algorithm' => 'none',
        ],
        'phpdoc_var_without_name' => true,
        'pow_to_exponentiation' => true,
        'pre_increment' => false,
        'protected_to_private' => false,
        'psr0' => ['dir' => __DIR__ . 'api/'],
        'psr4' => true,
        'random_api_migration' => [
            // TODO getrandmax -> mt_getrandmax, srand -> mt_srand
            'replacements' => [
                'rand' => 'random_int',
            ],
        ],
        'return_type_declaration' => ['space_before' => 'none'],
        // NOTE Replaces Foo::bar() to self::bar() instead of static::bar()
        'self_accessor' => true,
        'semicolon_after_instruction' => true,
        'short_scalar_cast' => true,
        'silenced_deprecation_error' => true,
        //'simplified_null_return' => true,
        'single_blank_line_at_eof' => true,
        'single_blank_line_before_namespace' => true,
        'single_class_element_per_statement' => [
            'elements' => [
                'const',
                'property',
            ],
        ],
        'single_import_per_statement' => true,
        'single_line_after_imports' => true,
        'single_line_comment_style' => [
            'comment_types' => ['asterisk', 'hash'],
        ],
        'single_quote' => true,
        'space_after_semicolon' => ['remove_in_empty_for_expressions' => true],
        'standardize_not_equals' => true,
        'strict_comparison' => true,
        'strict_param' => false,
        'switch_case_semicolon_to_colon' => true,
        'switch_case_space' => true,
        'ternary_operator_spaces' => true,
        'ternary_to_null_coalescing' => true,
        'trailing_comma_in_multiline_array' => true,
        'trim_array_spaces' => true,
        // NOTE Removes space between "- $foo" when it placed to show that value will be negative
        'unary_operator_spaces' => false,
        'visibility_required' => [
            'elements' => [
                'const',
                'property',
                'method',
            ],
        ],
        'void_return' => true,
        'whitespace_after_comma_in_array' => true,
        'yoda_style' => false,
    ]);
