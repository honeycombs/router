<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Router\Router;

use Honeycombs\Router\Router\Exception\InvalidUrlException;
use Honeycombs\Router\Router\Exception\RedirectException;
use Honeycombs\Router\Router\Exception\UnresolvedRouteException;
use Honeycombs\Router\Router\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    /**
     * Data provider provides routing maps, existing urls and resolved endpoint name
     *
     * @return array
     */
    public function getValidRoutingMap(): array
    {
        $emptyRoutingMap = [];
        $simpleRoutingMap = ['test' => 'testEndpoint'];
        $nestedRoutingMap = ['test' => ['%s=var1' => ['%d=var2' => 'nestedEndpointWithVariables']]];

        return [
            [$emptyRoutingMap, '/', 'index'],
            [$emptyRoutingMap, '/?some=value', 'index'],
            [$emptyRoutingMap, '', 'index'],
            [$emptyRoutingMap, '?some=value', 'index'],
            [$simpleRoutingMap, '/test/', 'testEndpoint'],
            [$simpleRoutingMap, '/test', 'testEndpoint'],
            [$simpleRoutingMap, 'test', 'testEndpoint'],
            [$nestedRoutingMap, 'test/one/2', 'nestedEndpointWithVariables', ['var1' => 'one', 'var2' => '2']],
            [$nestedRoutingMap, 'test/1/2', 'nestedEndpointWithVariables', ['var1' => '1', 'var2' => '2']],
        ];
    }

    /**
     * Data provider provides routing maps, existing urls and resolved endpoint name
     *
     * @return array
     */
    public function getValidNotEmptyRoutingMap(): array
    {
        $simpleRoutingMap = ['test' => 'testEndpoint'];
        $nestedRoutingMap = ['test' => ['%s=var1' => ['%d=var2' => 'nestedEndpointWithVariables']]];

        return [
            [$simpleRoutingMap, '/test/', 'testEndpoint'],
            [$simpleRoutingMap, '/test', 'testEndpoint'],
            [$simpleRoutingMap, 'test', 'testEndpoint'],
            [$nestedRoutingMap, 'test/one/2', 'nestedEndpointWithVariables', ['var1' => 'one', 'var2' => '2']],
            [$nestedRoutingMap, 'test/1/2', 'nestedEndpointWithVariables', ['var1' => '1', 'var2' => '2']],
        ];
    }

    /**
     * Data provider provides routing maps, unexisting urls for test 404
     *
     * @return array
     */
    public function getNotFoundRoutingMap(): array
    {
        $emptyRoutingMap = [];
        $simpleRoutingMap = ['test' => 'testEndpoint'];
        $nestedRoutingMap = ['test' => ['%s=var1' => ['%d=var2' => 'nestedEndpointWithVariables']]];

        return [
            [$emptyRoutingMap, 'wow'],
            [$emptyRoutingMap, '/wow/?some=value'],
            [$simpleRoutingMap, '/test1/'],
            [$simpleRoutingMap, 'tests', 'testEndpoint'],
            [$nestedRoutingMap, 'test/one/two', 'nestedEndpointWithVariables', ['var1' => 'one', 'var2' => '2']],
            [$nestedRoutingMap, 'test/1/2/3/4/5', 'nestedEndpointWithVariables', ['var1' => '1', 'var2' => '2']],
        ];
    }

    /**
     * Data provider provides routing maps, urls for test redirect with trailing slashes behaviour
     *
     * @return array
     */
    public function getRedirectsRoutingMap(): array
    {
        $emptyRoutingMap = [];

        return [
            [$emptyRoutingMap, 'wow', 'wow/', Router::TRAILING_SLASH_BEHAVIOUR_MUST],
            [$emptyRoutingMap, '/wow', '/wow/', Router::TRAILING_SLASH_BEHAVIOUR_MUST],
            [$emptyRoutingMap, '/wow/', '/wow', Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT],
            [$emptyRoutingMap, 'wow/', 'wow', Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT],
            [$emptyRoutingMap, 'wow?a=b', 'wow/?a=b', Router::TRAILING_SLASH_BEHAVIOUR_MUST],
            [$emptyRoutingMap, '/wow?a=b', '/wow/?a=b', Router::TRAILING_SLASH_BEHAVIOUR_MUST],
            [$emptyRoutingMap, '/wow/?a=b', '/wow?a=b', Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT],
            [$emptyRoutingMap, 'wow/?a=b', 'wow?a=b', Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT],
        ];
    }

    /**
     * Test resolving
     *
     * @param array $routingMap
     * @param string $testedUrl
     * @param string $expectedEndpointName
     * @param array $expectedVariables
     *
     * @dataProvider getValidRoutingMap
     * @covers ::resolveByUrl
     */
    public function testResolveByUrl(array $routingMap, string $testedUrl, string $expectedEndpointName, array $expectedVariables = []): void
    {
        $router = (new Router())->setRoutingMap($routingMap);
        $resolvedRoute = $router->resolveByUrl($testedUrl);
        $this->assertSame($resolvedRoute->getEndpointName(), $expectedEndpointName);
        $this->assertSame($resolvedRoute->getVariables(), $expectedVariables);
    }

    /**
     * Test getting 404 error
     *
     * @param array $routingMap
     * @param string $testedUrl
     * @throws \PHPUnit\Framework\Exception
     * @throws RedirectException
     * @throws UnresolvedRouteException
     *
     * @return void
     *
     * @dataProvider getNotFoundRoutingMap
     * @covers ::resolveByUrl
     */
    public function testNotFoundByUrl(array $routingMap, string $testedUrl): void
    {
        $router = (new Router())->setRoutingMap($routingMap);
        $this->expectExceptionMessage(sprintf('Cant resolve url: %s', $testedUrl));
        $this->expectExceptionCode(404);

        $router->resolveByUrl($testedUrl);
    }

    /**
     * Test getting redirect error
     *
     * @param array $routingMap
     * @param string $testedUrl
     * @param string $redirectUrl
     * @param int $trailingSlashesBehaviour
     * @throws InvalidUrlException
     * @throws UnresolvedRouteException
     * @throws \PHPUnit\Framework\Exception
     *
     * @return void
     *
     * @dataProvider getRedirectsRoutingMap
     * @covers ::resolveByUrl
     */
    public function testRedirectUrl(array $routingMap, string $testedUrl, string $redirectUrl, int $trailingSlashesBehaviour): void
    {
        $router = (new Router())->setRoutingMap($routingMap)->setTrailingSlashesBehaviour($trailingSlashesBehaviour);
        $this->expectExceptionMessage(sprintf('Redirect "%s" to proper url: "%s"', $testedUrl, $redirectUrl));
        $this->expectExceptionCode(301);

        $router->resolveByUrl($testedUrl);
    }

    /**
     * Test getting url by endpoint name with variables
     *
     * @throws \Honeycombs\Router\Router\Exception\UnregisteredPageException
     * @return void
     *
     * @covers ::getUrlByEndpointName
     */
    public function testGetUrlByEndpointNameWithVariables(): void
    {
        $endpointName = 'some';
        $routingMap = ['%s=var1' => ['%d=var2' => $endpointName]];
        $router = (new Router())->setRoutingMap($routingMap);
        $this->assertSame('/one/two', $router->getUrlByEndpointName($endpointName, ['var1' => 'one', 'var2' => 'two']));
    }

    /**
     * Test getting url by endpoint name
     *
     * @param array $routingMap
     * @param string $testedUrl
     * @param string $expectedEndpointName
     * @param array $expectedVariables
     * @throws \Honeycombs\Router\Router\Exception\UnregisteredPageException
     *
     * @dataProvider getValidNotEmptyRoutingMap
     *
     * @covers ::getUrlByEndpointName
     */
    public function testGetUrlByEndpointName(array $routingMap, string $testedUrl, string $expectedEndpointName, array $expectedVariables = []): void
    {
        $router = (new Router())->setRoutingMap($routingMap);
        $this->assertSame(trim($testedUrl, '/'), ltrim($router->getUrlByEndpointName($expectedEndpointName, $expectedVariables), '/'));
    }
}
