<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router\Exception;

class UnregisteredPageException extends \Exception
{
}
