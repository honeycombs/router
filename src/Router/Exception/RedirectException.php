<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router\Exception;

use Exception;
use Throwable;

/**
 * Class RedirectException
 * @todo tests
 */
class RedirectException extends Exception
{
    /**
     * Url to redirect to
     *
     * @var string
     */
    protected $redirectUrl;

    public function __construct(string $redirectUrl = '', string $originalUrl = '', int $code = 0, Throwable $previous = null)
    {
        $this->redirectUrl = $redirectUrl;
        parent::__construct(sprintf('Redirect "%s" to proper url: "%s"', $originalUrl, $redirectUrl), $code, $previous);
    }

    /**
     * Gets url to redirect to
     *
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }
}
