<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router\Exception;

use Throwable;

class UnresolvedRouteException extends \Exception
{
    /**
     * Illegal url
     *
     * @var string
     */
    protected $url;

    public function __construct(string $url = '', int $code = 0, Throwable $previous = null)
    {
        $this->url = $url;
        parent::__construct(sprintf('Cant resolve url: %s', $url), $code, $previous);
    }
}
