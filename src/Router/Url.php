<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router;

class Url
{
    /**
     * @var int
     */
    protected $trailingSlashBehaviour;

    /**
     * @var string
     */
    protected $originalUrl;

    /**
     * @var string[]
     */
    protected $urlParts;

    /**
     * @var string
     */
    protected $queryString;

    /**
     * @var string
     */
    protected $normalizedUrl;

    public function __construct(string $relativeUrl, $trailingSlashBehaviour = Router::TRAILING_SLASH_BEHAVIOUR_IGNORE)
    {
        $this->originalUrl = $relativeUrl;
        $this->trailingSlashBehaviour = $trailingSlashBehaviour;
        $this->prepareUrl();
    }

    /**
     * @return string
     */
    public function getOriginalUrl(): string
    {
        return $this->originalUrl;
    }

    /**
     * @return string[]
     */
    public function getUrlParts(): array
    {
        return $this->urlParts;
    }

    /**
     * @return string
     */
    public function getQueryString(): string
    {
        return $this->queryString;
    }

    /**
     * @return string
     */
    public function getNormalizedUrl(): string
    {
        return $this->normalizedUrl;
    }

    protected function prepareUrl(): void
    {
        $url = explode('?', $this->originalUrl, 2);
        $this->urlParts = explode('/', $url[0]);
        $this->queryString = $url[1] ?? '';

        foreach ($this->urlParts as $key => $urlPart) {
            if ($urlPart === '') {
                unset($this->urlParts[$key]);
            }
        }

        $this->normalizedUrl = implode('/', $this->urlParts);

        if (mb_substr($url[0], 0, 1) === '/') {
            $this->normalizedUrl = '/' . $this->normalizedUrl;
        }

        switch ($this->trailingSlashBehaviour) {
            case Router::TRAILING_SLASH_BEHAVIOUR_IGNORE:
                if (mb_strlen($url[0]) > 1 && mb_substr($url[0], -1) === '/') {
                    $this->normalizedUrl = $this->normalizedUrl . '/';
                }

                break;
            case Router::TRAILING_SLASH_BEHAVIOUR_MUST:
                $this->normalizedUrl .= '/';

                break;
            case Router::TRAILING_SLASH_BEHAVIOUR_MUST_NOT:
                break;
        }

        if ($this->queryString) {
            $this->normalizedUrl .= '?' . $this->queryString;
        }
    }
}
