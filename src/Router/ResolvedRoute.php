<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router;

/**
 * Class ResolvedRoute
 *
 * Class for storing resolved endpoint name and url variables
 */
class ResolvedRoute
{
    /**
     * Name of endpoint
     *
     * @var string
     */
    protected $endpointName;

    /**
     * Url variables
     *
     * @var array
     */
    protected $variables;

    /**
     * ResolvedRoute constructor.
     *
     * @param string $endpointName Name of resolved endpoint
     * @param array $variables url variables
     */
    public function __construct(string $endpointName, array $variables)
    {
        $this->endpointName = $endpointName;
        $this->variables = $variables;
    }

    /**
     * Gets resolved endpoint name
     *
     * @return string
     */
    public function getEndpointName(): string
    {
        return $this->endpointName;
    }

    /**
     * Gets url variables
     *
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }
}
