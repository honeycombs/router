<?php

declare(strict_types=1);

namespace Honeycombs\Router\Router;

use Honeycombs\Router\Router\Exception\RedirectException;
use Honeycombs\Router\Router\Exception\UnregisteredPageException;
use Honeycombs\Router\Router\Exception\UnresolvedRouteException;
use Kayex\HttpCodes;

/**
 * Class Router
 *
 * Router matches passed url to endpoint and generates urls for endpoints based on map
 */
class Router
{
    /**
     * Index page
     *
     * @const string
     */
    public const INDEX_PAGE = 'index';

    /**
     * Slashes must be on url ending
     *
     * @const string
     */
    public const TRAILING_SLASH_BEHAVIOUR_MUST = 1;

    /**
     * Slashes must not be on url ending
     *
     * @const string
     */
    public const TRAILING_SLASH_BEHAVIOUR_MUST_NOT = 2;

    /**
     * Slashes behaviour ignore
     *
     * @const string
     */
    public const TRAILING_SLASH_BEHAVIOUR_IGNORE = 3;

    /**
     * Routing map
     *
     * @var array
     */
    private $routingMap = [];

    /**
     * Trailing slashes behaviour
     *
     * @var int
     */
    private $trailingSlashesBehaviour = self::TRAILING_SLASH_BEHAVIOUR_IGNORE;

    /**
     * Prepared endpoints to generate urls
     *
     * @var string[]
     */
    protected $prepared;

    /**
     * @var array
     */
    protected $endpoints = [];

    /**
     * @param array $routingMap
     * @return Router
     */
    public function setRoutingMap(array $routingMap): self
    {
        $this->routingMap = $routingMap;

        return $this;
    }

    /**
     * @param int $trailingSlashesBehaviour
     * @return Router
     */
    public function setTrailingSlashesBehaviour(int $trailingSlashesBehaviour): self
    {
        $this->trailingSlashesBehaviour = $trailingSlashesBehaviour;

        return $this;
    }

    /**
     * Gets routing map
     *
     * @return array Routing map
     */
    public function getRoutingMap(): array
    {
        return $this->routingMap;
    }

    /**
     * Resolve passed url by rules from routing map
     *
     * @param string $requestUrl Relative url
     * @throws RedirectException
     * @throws UnresolvedRouteException If route not found
     * @return ResolvedRoute
     */
    public function resolveByUrl(string $requestUrl): ResolvedRoute
    {
        $url = new Url($requestUrl, $this->trailingSlashesBehaviour);
        $this->checkTrailingSlashes($url);

        $routingMap = $this->routingMap;
        $urlVariables = [];

        $urlParts = $url->getUrlParts();

        if (empty($urlParts)) {
            $pageId = self::INDEX_PAGE;
        } else {
            do {
                $pageId = $this->resolveLevel($routingMap, $urlParts, $urlVariables);
            } while (is_array($pageId));
        }

        if (empty($pageId)) {
            throw new UnresolvedRouteException($requestUrl, HttpCodes::HTTP_NOT_FOUND);
        }

        return new ResolvedRoute($pageId, $urlVariables);
    }

    /**
     * Generates relative url by pass
     *
     * @param string $endpointName
     * @param array $variables
     * @throws UnregisteredPageException
     * @return string
     */
    public function getUrlByEndpointName(string $endpointName, array $variables = []): string
    {
        if ($this->prepared === null) {
            $this->prepareRoutingMap();
        }

        $this->prepareRoutingMap();

        if (!isset($this->prepared[$endpointName])) {
            throw new UnregisteredPageException(sprintf('%s endpoint is not prepared', $endpointName));
        }

        return $this->generateUrl($endpointName, $variables);
    }

    /**
     * Generates url by endpointName and variables
     *
     * @param string $endpointName Endpoint name
     * @param array|null $variables Url variables
     * @return string Url
     */
    private function generateUrl(string $endpointName, array $variables = null)
    {
        $placeholder = $this->prepared[$endpointName];

        if (!empty($variables)) {
            foreach ($variables as $name => $value) {
                $placeholder = str_replace(['%s=' . $name, '%d=' . $name], $value, $placeholder);
            }
        }

        switch ($this->trailingSlashesBehaviour) {
            case self::TRAILING_SLASH_BEHAVIOUR_MUST:
                break;
            case self::TRAILING_SLASH_BEHAVIOUR_IGNORE:
                break;
            case self::TRAILING_SLASH_BEHAVIOUR_MUST_NOT:
                $placeholder = rtrim($placeholder, '/');

                break;
        }

        return $placeholder;
    }

    /**
     * Prepares urls for endpoints urls generation
     *
     * @return void
     */
    private function prepareRoutingMap(): void
    {
        $map = $this->getRoutingMap();
        $path = '';
        $this->walk($map, $path, $this->prepared);
    }

    /**
     * Recursive walks by routing map to prepare endpoint urls
     *
     * @param array|string $map Routing map or endpoint name (last leaf)
     * @param string $path Path
     * @param array|null $result Stores result
     */
    private function walk($map, string $path, array &$result = null): void
    {
        if (is_array($map)) {
            foreach ($map as $part => $value) {
                $path2 = $path . '/' . $part;
                $this->walk($value, $path2, $result);
            }
        } else {
            $result[$map] = $path;
        }
    }

    /**
     * Check that url trailing slash is valid
     *
     * @param Url $url
     * @throws \Honeycombs\Router\Router\Exception\RedirectException
     *
     * @return void
     */
    protected function checkTrailingSlashes(Url $url): void
    {
        $normalizedUrl = $url->getNormalizedUrl();
        $originalUrl = $url->getOriginalUrl();

        if ($normalizedUrl !== $originalUrl) {
            throw new RedirectException($normalizedUrl, $url->getOriginalUrl(), HttpCodes::HTTP_MOVED_PERMANENTLY);
        }
    }

    /**
     * Recursive walking in map founding target page using passed url parts. Fllls variables parameter
     *
     * @param array|string $routingMap Routing map
     * @param array $requestUriParts Request url exploded by "/"
     * @param array $variables Variables filling with request url parameters
     *
     * @return string|null
     */
    private function resolveLevel(&$routingMap, array &$requestUriParts, array &$variables = []): ?string
    {
        // cant go deeper than exists
        if (!is_array($routingMap) && !empty($requestUriParts)) {
            return null;
        }

        // found endpoint
        if (!is_array($routingMap)) {
            return $routingMap;
        }
        $currentRequestUriPart = '';

        while (empty($currentRequestUriPart) && !empty($requestUriParts)) {
            $currentRequestUriPart = array_shift($requestUriParts);
        }

        if (isset($routingMap[$currentRequestUriPart])) {
            return $this->resolveLevel($routingMap[$currentRequestUriPart], $requestUriParts, $variables);
        }

        foreach ($routingMap as $siteMapId => $siteMapPart) {
            $variableName = null;
            [$ruleKey, $variableName] = $this->extractRuleKeyAndVariableName($siteMapId);

            if (!$ruleKey) {
                $ruleKey = $siteMapId;
            }

            if (
                ($ruleKey === '%d') && ((string) (int) $currentRequestUriPart === $currentRequestUriPart)
                ||
                ($ruleKey === '%s' && $currentRequestUriPart !== '')
            ) {
                if ($variableName) {
                    $variables[$variableName] = $currentRequestUriPart;
                }

                return $this->resolveLevel($routingMap[$siteMapId], $requestUriParts, $variables);
            }
        }

        return null;
    }

    /**
     * @param string $ruleKey
     * @return array
     */
    private function extractRuleKeyAndVariableName(string $ruleKey)
    {
        $variableName = null;

        if (!empty($ruleKey[0]) && '%' === $ruleKey[0] && strlen($ruleKey) > 3) {
            $variableName = substr($ruleKey, 3);
            $ruleKey = substr($ruleKey, 0, 2);
        }

        return [$ruleKey, $variableName];
    }
}
