# README #

Router library to find routes

### What is this repository for? ###

Router can parse routes configuration to resolve url to endpoint.
In additional, router can use configuration to generate urls from endpoints

### How do I get set up? ###

Just put in your composer.json 
 "require": {
    "honeycombs/router" : "0.0.*"
  },

### Support ###

amuhc@yandex.ru